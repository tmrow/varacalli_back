@extends('index')

@section('content')
    <!-- Muestra todo lo referente a los camiones -->
<?php $camiones = json_decode($trucks, true);
?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tables</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Camiones registrados
                </div>
                <div>
                    <button class="btn btn-success" data-toggle="modal" data-target="#agregarCamion">Agregar nuevo</button>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Patente</th>
                                <th>Modelo</th>
                                <th>Capacidad</th>
                                <th>Telefono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                            //Foreach para recorrer todos los camiones 
                            foreach ($camiones as $camion => $value) { ?>
                            <!--Inicio Fila del camion-->
                            <tr class="odd gradeX">
                            <td>{{$value['id']}}</td>
                                <td>{{$value['patent']}}</td>
                                <td>{{$value['model']}}</td>
                                <td class="center">{{$value['capacity']}}</td>
                                <td class="center">{{$value['phone']}}</td>
                                <td>
                                    <div>
                                        <button class="btn btn-success" data-toggle="modal" data-target="#modificarCamion{{$value['id']}}" identifier={{$value['id']}}>Modificar</button>
                                    </div>
                                    <div>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#eliminarCamion{{$value['id']}}">Eliminar</button>
                                    </div> 
                                </td>
                            </tr>
                            <!--Fin Fila del camion -->
                            <!--Inicio Modal para editar el camion de esa fila-->
                            <div class="modal fade" id="modificarCamion{{$value['id']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Registrar Camion</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ action('API\TruckController@update', ['id'=>$value['id']]) }}" method="post">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="patent">Patente</label>
                                                    <input type="text" class="form-control" id="patent" name="patent" value="{{$value['patent']}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="model">Modelo</label>
                                                    <input type="text" class="form-control" id="model" name="model" value="{{$value['model']}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="capacity">Capacidad</label>
                                                    <input type="text" class="form-control" id="capacity" name="capacity" value="{{$value['capacity']}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone">Telefono</label>
                                                    <input type="text" class="form-control" id="phone" name="phone" value="{{$value['phone']}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="rol_id">Rol</label>
                                                    <input type="text" class="form-control" id="rol_id" name="rol_id" value="{{$value['rol_id']}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                                    <button type="submit" class="btn btn-success">Guardar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal -->
                            <!--Inicio Modal para eliminar el camion devuelve un error-->
                            <div class="modal fade" id="eliminarCamion{{$value['id']}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Registrar Camion</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ action('API\TruckController@destroy', ['id'=>$value['id']]) }}" method="get">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label for="patent">¿Deasea eliminar el camion seleccionado?</label>
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                                    <button type="submit" class="btn btn-success">Borrar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Fin Modal -->
                            <?php }
                            ?> 
                        </tbody>
                    </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- Modal para agregar un nievo camion-->
    <div class="modal fade" id="agregarCamion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Registrar Camion</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ action('API\TruckController@store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="patent">Patente</label>
                            <input type="text" class="form-control" id="patent" name="patent">
                        </div>
                        <div class="form-group">
                            <label for="model">Modelo</label>
                            <input type="text" class="form-control" id="model" name="model">
                        </div>
                        <div class="form-group">
                            <label for="capacity">Capacidad</label>
                            <input type="text" class="form-control" id="capacity" name="capacity">
                        </div>
                        <div class="form-group">
                            <label for="phone">Telefono</label>
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                        <div class="form-group">
                            <label for="rol_id">Rol</label>
                            <input type="text" class="form-control" id="rol_id" name="rol_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Guardar</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
    <!--Fin modal-->

    <!--En este codigo se encuentran totalmente funcionales las opciones de guardar, editar y la muestra de todos los camiones, 
        la opcion de eliminar genera un conflicto, se modifico la base de datos se le coloco a las claves foraneas la propiedad 
        onDelete('cascade'), este cambio no fue testeado -->
    
@stop
