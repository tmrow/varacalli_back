<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Truck;
//LAs rutas de la api, devuelven un json 
Route::get('/', function () {
    return view('home');
});
Route::get('/pedidos', function () {
    return view('pedidos');
});
Route::get('/usuarios', function () {
    return view('usuarios');
});
Route::get('/productos', function () {
    return view('productos');
});
Route::get('/camiones', function () {
    $trucks = Truck::all();
    return view('camiones', ['trucks' => $trucks]);
});
Route::get('/zonas', function () {
    return view('zonas');
});
Route::get('/reportes', function () {
    return view('reportes');
});


