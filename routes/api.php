<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user', 'API\UserController@register');

//Rol API Routes

Route::get('rol', 'API\RolController@index');

Route::post('rol', 'API\RolController@store');

Route::get('rol/{id}', 'API\RolController@show');

Route::put('rol/{id}', 'API\RolController@update');

Route::delete('rol/{id}', 'API\RolController@destroy');

//Address API Routes

Route::get('user/address/{id}', 'API\AddressController@index');

Route::post('address', 'API\AddressController@store');

Route::get('address/{id}', 'API\AddressController@show');

Route::put('address/{id}', 'API\AddressController@update');

Route::delete('address/{id}', 'API\AddressController@destroy');

//Order Api Routes

Route::get('user/order/{id}', 'API\OrderController@index');

Route::post('order', 'API\OrderController@store');

Route::get('order/{id}', 'API\OrderController@show');

Route::put('order/{id}', 'API\OrderController@update');

Route::delete('order/{id}', 'API\OrderController@destroy');

//Product API Routes

Route::get('products', 'API\ProductController@index');

Route::post('product', 'API\ProductController@store');

Route::get('product/{id}', 'API\ProductController@show');

Route::put('product/{id}', 'API\ProductController@update');

Route::delete('product/{id}', 'API\ProductController@destroy');

//Promotion API Routes

Route::get('promotions', 'API\PromotionController@index');

Route::post('promotion', 'API\PromotionController@store');

Route::get('promotion/{id}', 'API\PromotionController@show');

Route::put('promotion/{id}', 'API\PromotionController@update');

Route::delete('promotion/{id}', 'API\PromotionController@destroy');

//Truck API Routes

Route::get('trucks', 'API\TruckController@index');

Route::post('truck', 'API\TruckController@store');

Route::get('struck/{id}', 'API\TruckController@show');

Route::post('truck/{id}', 'API\TruckController@update');

Route::get('truck/{id}', 'API\TruckController@destroy');

//Schedule API Routes

Route::get('schedules', 'API\ScheduleController@index');

Route::post('schedule', 'API\ScheduleController@store');

Route::get('schedule/{id}', 'API\ScheduleController@show');

Route::put('schedule/{id}', 'API\ScheduleController@update');

Route::delete('schedule/{id}', 'API\ScheduleController@destroy');

//Setting API Routes

Route::get('settings', 'API\SettingController@index');

Route::post('setting', 'API\SettingController@store');

Route::get('setting/{id}', 'API\SettingController@show');

Route::put('setting/{id}', 'API\SettingController@update');

Route::delete('setting/{id}', 'API\SettingController@destroy');

//Vehicle API Routes

Route::get('vehicles', 'API\VehicleController@index');

Route::post('vehicle', 'API\VehicleController@store');

Route::get('vehicle/{id}', 'API\VehicleController@show');

Route::put('vehicle/{id}', 'API\VehicleController@update');

Route::delete('vehicle/{id}', 'API\VehicleController@destroy');

//Zone API Routes

Route::get('zones', 'API\ZoneController@index');

Route::post('zone', 'API\ZoneController@store');

Route::get('zone/{id}', 'API\ZoneController@show');

Route::put('zone/{id}', 'API\ZoneController@update');

Route::delete('zone/{id}', 'API\ZoneController@destroy');