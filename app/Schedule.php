<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de horario
class Schedule extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'description', 'date', 'hours', 'status', 'truck_id',
    ];
    //Relacion con la tabla camiones
    public function truck()
    {
        return $this->hasOne('App\Truck');
    }
}
