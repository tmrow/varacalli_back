<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de pedidos
class Order extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'location', 'schedule', 'status', 'cantLts', 'finalCost', 'observations', 'user_id', 'product_id', 'truck_id',
    ];
    //Relacion con la tabla usuarios
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    //Relacion con la tabla productos
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    //Relacion con la tabla camiones
    public function truck()
    {
        return $this->belongsTo('App\Truck');
    }
}
