<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Zone;
//Controlador de zonas
class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar todos las zonas guardadas
    public function index()
    {
        $zones = Zone::all();

        return response()->json($zones, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Funcion para guardar zonas
    public function store(Request $request)
    {
        $zone = Zone::create($request->all());

        return response()->json($zone, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar una zona especifica
    public function show($id)
    {
        $zone = Zone::find($id);

        return response()->json($zone, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para actualizar una zona
    public function update(Request $request, $id)
    {
        $zone = Zone::findOrFail($id);
        $zone->update($request->all());

        return response()->json($zone, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para eliminar una zona
    public function destroy($id)
    {
        $zone = Zone::find($id)->delete();

        return response()->json($zone, 200);
    }
}
