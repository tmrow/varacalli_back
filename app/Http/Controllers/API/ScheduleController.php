<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
//Controlador de horarios 
class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar todos los horarios guardados
    public function index()
    {
        $schedules = Schedule::all();

        return response()->json($schedules, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Funcion para guardar horarios
    public function store(Request $request)
    {
        $schedule = Schedule::create($request->all());

        return response()->json($schedule, 201);    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar un horario especifico
    public function show($id)
    {
        $schedule = Schedule::find($id);

        return response()->json($schedule, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para actualizar un horario
    public function update(Request $request, $id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->update($request->all());

        return response()->json($schedule, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para eliminar un horario
    public function destroy($id)
    {
        $schedule = Schedule::find($id)->delete();

        return response()->json($schedule, 200);
    }
}
