<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
//Controlador de promociones 
class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar todos las promociones guardadas
    public function index()
    {
        $promotions = Promotion::all();

        return response()->json($promotions, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Funcion para guardar promociones
    public function store(Request $request)
    {
        $promotion = Promotion::create($request->all());

        return response()->json($promotion, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar una promocion especifica
    public function show($id)
    {
        $promotion = Promotion::find($id);

        return response()->json($promotion, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para actualizar una promocion
    public function update(Request $request, $id)
    {
        $promotion = $id->update($request->all());

        return response()->json($promotion, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para eliminar una promocion
    public function destroy($id)
    {
        $promotion = Promotion::destroy($id);

        return response()->json($promotion, 200);
    }
}
