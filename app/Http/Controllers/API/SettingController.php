<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
//Controlador de direcciones 
class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar todos los ajustes guardados
    public function index()
    {
        $settings = Setting::all();

        return response()->json($settings, 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Funcion para guardar ajustes
    public function store(Request $request)
    {
        $setting = Setting::create($request->all());

        return response()->json($setting, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar un ajuste especifico
    public function show($id)
    {
        $setting = Setting::find($id);

        return response()->json($setting, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para actualizar un ajuste
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);
        $setting->update($request->all());

        return response()->json($setting, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para eliminar un ajuste
    public function destroy($id)
    {
        $setting = Setting::find($id)->delete();

        return response()->json($setting, 200);

    }
}
