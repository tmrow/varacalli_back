<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Address;
Use Validator;
//Controlador de direcciones 
class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar todos las direcciones guardadas
    public function index($id)
    {
        $addresses = Address::all()->where('user_id', $id);

        return response()->json($addresses, 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Funcion para guardar direcciones
    public function store(Request $request)
    {
        $address = Address::create($request->all());

        return response()->json($address, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para mostrar una direccion especifica
    public function show($id)
    {
        $address = Address::find($id);

        return response()->json($address, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para actualizar una direccion
    public function update(Request $request, $id)
    {
        
        $address = Address::findOrFail($id);
        $address->update($request->all());

        return response()->json($address, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Funcion para eliminar una direccion
    public function destroy($id)
    {
        $address = Address::find($id)->delete();

        return response()->json($address, 200);
    }
}
