<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de promocion
class Promotion extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'name', 'description', 'cantLts', 'price', 'validity', 'active', 'activity', 'points', 'product_id',
    ];
    //Relacion con la tabla productos
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    //Relacion con la tabla usuarios
    public function user()
    {
        return $this->belongsToMany('App\User', 'promotions_users_table', 'user_id', 'promotion_id');
    }
}
