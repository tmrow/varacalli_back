<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
//Modelo de usuario
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //Campos a indexar en la base de datos
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'points', 'status', 'rol_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //Campos protegidos a indexar
    protected $hidden = [
        'password', 'remember_token',
    ];
    //Relacion con la tabla promociones
    public function promotion()
    {
        return $this->belongsToMany('App\Promotion', 'promotions_users_table', 'user_id', 'promotion_id');
    }
    //Relacion con la tabla vahiculos
    public function vehicle()
    {
        return $this->hasMany('App\Vehicle');
    }
    //Relacion con la tabla direcciones
    public function address()
    {
        return $this->hasMany('App\Address');
    }
    //Relacion con la tabla ordenes
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    //Relacion con la tabla roles
    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }
}
