<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de zona
class Zone extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'description', 'truck_id',
    ];
    //Relacion con la tabla camiones
    public function truck()
    {
        return $this->hasOne('App\Truck');
    }
}
