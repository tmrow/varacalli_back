<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de direcciones
class Address extends Model
{
    //Campos a indexar en base de datos
    protected $fillable = [
        'latitude','length','description', 'location', 'user_id',
    ];
    //Relacion con tabla usuarios
    public function user()
    {
        return $this->belongsTo('App/User');
    }
}
