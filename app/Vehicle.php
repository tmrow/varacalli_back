<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de vehiculo
class Vehicle extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'model', 'brand', 'patent', 'category', 'gasType', 'user_id',
    ];
    //Relacion con la tabla usuarios
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
