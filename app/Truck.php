<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de camiones
class Truck extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'patent', 'model', 'capacity', 'phone', 'rol_id',
    ];
    //Relacion con la tabla zonas
    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }
    //Relacion con la tabla pedidos 
    public function order()
    {
        return $this->hasMany('App\Order', 'foreign_key', 'local_key');
    }
    //Relacion con la tabla horarios
    public function schedule()
    {
        return $this->belongsTo('App\Schedule');
    }
}
