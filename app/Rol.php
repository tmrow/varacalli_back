<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de rol
class Rol extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'name',
    ];
    //Relacion con la tabla usuarios
    public function user()
    {
        return $this->hasMany('App\User');
    }
    //Relacion con la tabla ajustes
    public function setting()
    {
        return $this->hasOne('App\Setting');
    }
}
