<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de ajustes
class Setting extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'zone', 'schedule', 'rol_id',
    ];
    //Relacion con la tabla rols
    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }
}
