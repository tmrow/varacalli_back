<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//Modelo de Producto
class Product extends Model
{
    //Campos a indexar en la base de datos
    protected $fillable = [
        'name', 'price', 'category', 'minLts', 'maxLts', 'active', 'points',
    ];
    //Relacion con la tabla pedidos
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    //Relacion con la tabla promociones
    public function promotion()
    {
        return $this->hasMany('App\Promotion');
    }
}
